<?php

$forbidens = ['.', '..', basename(__FILE__)];

foreach(scandir(__DIR__) as $file) {
    if (! in_array($file, $forbidens)) {
        foreach (
        (require_once dirname(__DIR__) . '/config/' . $file)
            as $key => $value
        ) {
            defined(strtoupper($key)) || define(strtoupper($key), $value);
        }
    }
}
