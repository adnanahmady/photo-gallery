<?php

use App\Core\Route;

Route::get('/params/{id}/mypaths/{param}', 'HomeController@test');
Route::post('/params/{id}/mypaths/{param}', 'HomeController@testPost');
