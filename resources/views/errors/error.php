<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exception: <?php echo $exception; ?></title>
    <link type="text/css" rel="stylesheet" href="<?php echo BASE_URL; ?>/public/css/app.css">
</head>
<body>
<div class="container bg-light border border-secondary rounded">
    <header class="header row bg-warning">
        <div class="col">
            <div class="bg-danger row h1 design-1 my-0 text-light">
                <div class="col py-4">
                    <strong class="mr-2">Exception:</strong><span><?php echo $exception; ?></span>
                </div>
            </div>
            <div class="bg-warning row h4 my-0 ">
                <div class="col py-3">
                    <strong class="mr-2">Message:</strong><span><?php echo $message; ?></span>
                </div>
            </div>
        </div>
    </header>
    <main class="main row">
        <div class="col py-3">
            <div class="row text-dark">
                <div class="col">
                    <span class="mr-1"><?php echo $file; ?></span>
                    <strong>
                        (<?php echo $line; ?>)
                    </strong>
                </div>
            </div>
            <div class="row text-dark">
                <div class="col">
                    <span class="mr-1">Code:</span><code><?php echo $code; ?></code>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php $my = $stackTrace; ?>
                    <?php while($row = array_shift($stackTrace)): ?>
                        <div class="row text-dark">
                            <div class="col">
                                <span class="mr-1">File:</span>
                                <code>
                                    <?php echo $row['file']; ?>
                                    <strong>(<?php echo $row['line']; ?>)</strong>
                                </code>
                            </div>
                        </div>
                        <div class="row text-dark">
                            <div class="col">
                                <span class="mr-1">Class:</span>
                                <code><?php echo $row['class']; ?></code>
                                <code><?php echo $row['type']; ?></code>
                                <code><?php echo $row['function']; ?></code>
                                <code>(
                                    <?php
                                    $args = end(array_values($row['args']));
                                    if (is_array($args)) {
                                        foreach($args as $key => $value) {
                                            echo $key, ': ',$value;
                                            if (end($args) !== $value) {
                                                echo ', ';
                                            }
                                        }
                                    } else {
                                        echo $args;
                                    } ?>
                                    )</code>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </main>
</div>
</body>
</html>