<?php

function view($uniqueViewFile, $params = []) {
    foreach($params as $key => $value) {
        $$key = $value;
    }

    require_once dirname(__DIR__) . '/resources/views/' . $uniqueViewFile . '.php';
}

function getVarName($var) {
    foreach($GLOBALS as $varName => $value) {
        if ($value === $var) {
            return $varName;
        }
    }

    return null;
}