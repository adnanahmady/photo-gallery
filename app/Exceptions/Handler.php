<?php

namespace App\Exceptions;

use App\Core\ExceptionHandler;

class Handler extends ExceptionHandler
{
    public function handle(\Throwable $e) {
        ($this->debug($e));
    }
}