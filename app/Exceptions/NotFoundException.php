<?php
/**
 * Created by PhpStorm.
 * User: adnan
 * Date: 6/4/19
 * Time: 4:36 PM
 */

namespace App\Exceptions;

use Throwable;

class NotFoundException extends \Exception
{
    public function __construct(string $message = "", int $code = 404, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}