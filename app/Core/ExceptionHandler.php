<?php

namespace App\Core;

class ExceptionHandler
{
    protected $exception;

    public function __invoke(\Throwable $e)
    {
        $this->handle($e);
    }

    public function __debugInfo()
    {
        return [
            'Message' => $this->exception->getMessage(),
            'File' => $this->exception->getFile(),
            'Line' => $this->exception->getLine(),
            'Code' => $this->exception->getCode(),
            'StackTrace' => $this->exception->getTrace(),
        ];
    }

    public static function __callStatic($name, $arguments)
    {
        $reflectionMethod = new \ReflectionMethod(self, $name);
        $invokeArgs = $reflectionMethod->invokeArgs(new self, $arguments);
    }

    public function handle(\Throwable $e) {
        $this->debug($e);
    }

    protected function debug(\Throwable $e) {
        $this->exception = $e;
        $error = $this->getExceptionView('errors/error', $e->getCode());
        view($error, [
            'exception'   => end(explode('\\', get_class($e))),
            'message'     => $e->getMessage(),
            'file'        => $e->getFile(),
            'line'        => $e->getLine(),
            'code'        => $e->getCode(),
            'stackTrace'  => $e->getTrace(),
        ]);

        return $this;
    }

    protected function getExceptionView($file, $code) {
        if (file_exists(
                dirname(__DIR__, 2) . '/resources/views/' . $file . '-' . $code . '.php'
            )):
            return $file . '-' . $code;
        endif;

        return $file;
    }
}