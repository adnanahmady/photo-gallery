<?php

namespace App\Core;

class Model {
    protected static $model;
    protected $connection;
    protected $table;
    protected $fields = [];

    public static function connect()
    {
        if (static::$model === null) {
            static::$model = new static;

            static::$model->connection = Connection::connect()->conn;
            static::$model->connection->setAttribute(
                \PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION
            );
        }

        return static::$model;
    }

    protected function getTable()
    {
        if (static::connect()->table === null) {
            static::connect()->table = strtolower(basename(
                        end(
                            explode(
                                '\\',
                                get_called_class()
                            )
                        )
                    )
                ) . 's';
        }

        return static::connect()->table;
    }

    public static function __callStatic($name, $arguments)
    {
        $reflactionMethod = new \ReflectionMethod(static::$model, $name);
        $reflactionMethod->invokeArgs(new static::$model, $arguments);
    }

    public function getFields() {
        return empty(static::$fields) ? '*' : implode(', ', static::$fields);
    }

    public function all() {
        $result = static::prepare(
            'SELECT ' . static::getFields() . ' FROM ' . static::getTable()
        );
        $result->execute();

        return $result->fetchAll();
    }

    public function find($id) {
        $result = static::prepare(
            'SELECT ' . static::getFields() . ' FROM ' . static::getTable() . ' WHERE id = :id'
        );
        $result->execute([':id' => $id]);

        return $result->fetch();
    }

    public function __debugInfo()
    {
        $fields = [];
        foreach($this->fields as $value) {
            $fields[$value] = $this->$value;
        }

        return $fields;
    }

    public function prepare($query) {
        $result = static::connect()->connection->prepare($query);
        $result->setFetchMode(\PDO::FETCH_CLASS, get_called_class());

        return $result;
    }

    public function query($sql) {
        $result = static::connect()->connection->query(
            str_replace(':table', static::getTable(), $sql)
        );
        $result->setFetchMode(\PDO::FETCH_CLASS, get_called_class());

        return $result->execute();
    }

    private function __clone(){}
    private function __wakeup(){}
}