<?php

namespace App\Core;

class Connection
{
    protected static $connection;
    public $conn;

    public static function connect()
    {
        if (static::$connection === null) {
            static::$connection = new static;

            try {
                static::$connection->conn = new \PDO(
                    sprintf(
                        '%1$s:host=%2$s;dbname=%3$s',
                        DB_CONNECTION,
                        DB_SERVER,
                        DB_NAME
                    ),
                    DB_USER,
                    DB_PASS
                );

                static::$connection->conn->setAttribute(
                    \PDO::ATTR_ERRMODE,
                    \PDO::ERRMODE_EXCEPTION
                );

            } catch (\PDOException $e) {
                echo 'connection failed: ' . $e->getMessage();
                die;
            }
        }

        return static::$connection;
    }

    protected function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}