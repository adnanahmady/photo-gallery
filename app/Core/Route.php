<?php

namespace App\Core;

class Route
{
    private static $route;
    private static $routes = [];
    public const ALLOWED_REQUEST_METHODS = [
        'GET',
        'PUT',
        'POST',
        'PATCH',
        'DELETE'
    ];

    public static function __callStatic($allowedMethod, $arguments) {
        $route = static::Route();
        $pattern = $route->getPattern($arguments);

        $executor = explode('@',
            current(preg_grep('/@/', $arguments))
        );

        $controller = current($executor);
        $method = end($executor);

        array_push(
            static::$routes,
            [
                'route'      => $arguments[0],
                'pattern'    => $pattern,
                'controller' => $controller,
                'method'     => $method,
                'allowed_method' => $allowedMethod
            ]
        );
    }

    public static function getRoutes() {
        return static::$routes;
    }

    public static function Route(): Route
    {
        if (static::$route === null) {
            static::$route = new static;
        }

        return static::$route;
    }

    protected function getPattern($arguments): string
    {
        $pattern = implode(
            '\/',
            preg_replace(
                '/{[\w]+}/',
                '[\w]+',
                explode(
                    '/',
                    $arguments[0]
                )
            )
        );

        return $pattern;
    }

    protected function __construct(){}
    public function __clone(){}
    public function __wakeup(){}
}
