<?php

namespace App\Core;

use App\Exceptions\Handler;
use App\Exceptions\NotFoundException;

class Core
{
    public function __construct()
    {
        list($result, $type) = $this->checkPublic();

        if (false !== $result) {
            header('Content-Type: ' . $type);
            require_once dirname(__DIR__, 2) . $result;
            return true;
        }
        try {
            $route = $this->findRoute();
        } catch (\Throwable $e) {
            (new Handler())->handle($e);
            return true;
        }
        $params = $this->getParams($route['route']);
        $controller = '\\App\\Http\\Controllers\\' . $route['controller'];
        $method = $route['method'];

        try {
            $reflectionMethod = new \ReflectionMethod($controller, $method);
            $invokeArgs = $reflectionMethod->invokeArgs(new $controller(), $params);
            if ($invokeArgs !== null) {
                echo is_array($invokeArgs) ?
                    json_encode($invokeArgs) :
                    $invokeArgs;
            }
        } catch (\Throwable $e) {
            (new Handler())->handle($e);
        }
    }

    protected function checkPublic()
    {
        $type = 'font/opentype';

        if (preg_match(
            '/\.css$/',
            $_SERVER['REQUEST_URI']
        )) {
            $type = 'text/css';
        } elseif (preg_match(
            '/\.js$/',
            $_SERVER['REQUEST_URI']
        )) {
            $type = 'application/javascript';
        }

        if (preg_match(
            '/^\/public\//i',
            $_SERVER['REQUEST_URI']
        )) {
            return [
                $_SERVER['REQUEST_URI'],
                $type
            ];
        }

        return [false, $type];
    }

    protected function getParams($argument): array
    {
        $grep =
            str_replace('{', '',
                str_replace('}', '',
                    preg_grep(
                        '/{(.)+}/',
                        explode(
                            '/',
                            $argument
                        )
                    )
                )
            );
        $url = explode('/', $_SERVER['REQUEST_URI']);
        $return = [];
        foreach ($grep as $key => $value) {
            $return[$value] = $url[$key];
        }

        return $return;
    }

    private function checkMethod($method): bool
    {
        return (
            in_array(
                strtoupper($method),
                Route::ALLOWED_REQUEST_METHODS
            ) &&
            strtoupper($method) == $_SERVER['REQUEST_METHOD']
        ) ? true : false;
    }

    private function findMethod($method): void
    {
        if (
            ! in_array(
                strtoupper($method),
                Route::ALLOWED_REQUEST_METHODS
            ) &&
            strtoupper($method) == $_SERVER['REQUEST_METHOD']
        ) {
            throw new \Exception('Un Allowed Request Method');
        }
    }

    protected function checkRoute(string $pattern): bool
    {
        return preg_match(
            "/^{$pattern}$/i",
            $_SERVER['REQUEST_URI']
        ) ? true : false;
    }

    protected function findRoute()
    {
        foreach(Route::getRoutes() as $value) {
            if ($this->checkRoute($value['pattern']) &&
                $this->checkMethod($value['allowed_method'])) {
                $route = $value;
            }
        }

        if (! isset($route)) {
            throw new NotFoundException('Resource Not Found');
        }

        return $route;
    }
}
