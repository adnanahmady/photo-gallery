<?php
//header('Access-Control-Allow-Origin: *');
require_once dirname(__DIR__) . '/vendor/autoload.php';
require_once dirname(__DIR__) . '/config/config.php';
require_once dirname(__DIR__) . '/routes/Routes.php';
require_once dirname(__DIR__) . '/helpers/functions.php';

use App\Core\Core;

$core = new Core();
