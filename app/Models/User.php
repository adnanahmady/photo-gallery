<?php

namespace App\Models;

use App\Core\Model;

class User extends Model
{
    protected $fields = [
        'id',
        'username',
        'password',
        'first_name',
        'last_name',
    ];
}